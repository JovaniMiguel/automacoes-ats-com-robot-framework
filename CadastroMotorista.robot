*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${BROWSER}             chrome
${4fordevs}            chrome
${Gerador_cnh}         chrome
${URL}                 http://atsweb.sistemainfo.com.br:8024/login
${URLb}                https://www.4devs.com.br/ 
${URLcnh}              https://geradornv.com.br/gerador-cnh/                       
${local_teste}=        xpath=/html[1]/body[1]/header[1]/div[1]/div[1]/form[1]/input[1]
${cpf} 
${nome}
${resp_nome}           
${resp_cpf}
${rg}
${resp_rg}
${cnh}
${resp_cnh}



*** Keywords *** 

Abrir o site do gerador de CNH
   Open Browser  url=${URLcnh}    browser=${Gerador_cnh}

Clicar no botao para gerar cnh
  Click Button        xpath=//*[@id="nv-new-generator-cnh"]
  
copiar a cnh eescrever no console
   ${cnh}   Get Text   xpath=//*[@id="nv-field-generator-cnh"]
   log to console    ${cnh}
   ${resp_cnh}=  Set Suite Variable   ${cnh}




*** Keywords *** 
Acessar a pagina do site do 4fordevs
   Open Browser  url=${URLb}   browser=${4fordevs} 

Clicar no botao gerador de pessoas
   Click Element   xpath=//*[@id="top-nav"]/li[23]/a

Clicar no checkbox para não gerar com pntuação 
   Click Element      xpath=/html[1]/body[1]/main[1]/div[1]/div[2]/div[1]/div[4]/div[1]/div[2]/div[2]/div[4]/div[1]/div[3]/label[1]/span[1]

Clicar no checkbox para criar um usuário do masculino
   Click Element      xpath=//*[@id="app-wrapper"]/div[2]/div[2]/div[1]/div[1]/div[2]/label/span

Clicar no botão gerar pessoa
  Click Button        xpath=/html[1]/body[1]/main[1]/div[1]/div[2]/div[1]/div[4]/div[1]/div[3]/label[1]/input[1]
  sleep  3s  
  Set Selenium Implicit Wait  5s
  Maximize Browser window

Resgatar o nome
  ${nome}   Get Text   xpath=//*[@id="nome"]/span[1]
  log to console       ${nome}
  ${resp_nome}=        Set Suite Variable   ${nome}

Resgatar o cpf
  ${cpf}   Get Text   xpath=//*[@id="cpf"]/span[1]
  log to console   ${cpf}
  ${resp_cpf}=  Set Suite Variable   ${cpf}

Resgatar o RG
  ${rg}   Get Text   xpath=//*[@id="rg"]/span[1]
  log to console    ${rg}
  ${resp_rg}=  Set Suite Variable   ${rg}
                    
Passar valor da variavel para o campo 
  ${resp_nome}=  Set Suite Variable   ${nome}
  Input Text   xpath=//*[@id="search-input"]     ${nome}



   
*** Keywords ***

Acessar a pagina home do ATS
   Open Browser  url=${URL}   browser=${BROWSER} 

Clicar no botão Cokies
  Click Element   xpath=(.//*[normalize-space(text()) and normalize-space(.)='Lei Geral de Proteção de Dados'])[2]/following::button[1]

Digitar o nome "${USUARIO}" na Pesquisa
  Input Text      xpath=/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/form[1]/input[1]    ${USUARIO}
 
Informar a senha "${Senha login}" no campo senha
   Input Text     xpath=/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/form[1]/input[2]     ${Senha login}
  
Clicar no botão subimit
   Click Button   xpath=/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/form[1]/button[1]
   sleep  5s  
   Set Selenium Implicit Wait  8s
  
Clicar no botão config
   Click Element      xpath=/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/nav[1]/div[1]/span[1]/div[1]/ul[1]/li[2]/a[1]/span[1]

Clicar no botão cadastros
   Click Element      xpath=//*[@id="side-menu"]/li[2]/ul/li/a
   sleep  2s  
   Set Selenium Implicit Wait  4s
Acessar o campo motoristas
   Click Element      xpath=/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/nav[1]/div[1]/span[1]/div[1]/ul[1]/li[2]/ul[1]/li[1]/a[1]

Clicar no campo para cadastrar
   Click Element      xpath=/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/nav[1]/div[1]/span[1]/div[1]/ul[1]/li[2]/ul[1]/li[1]/ul[1]/li[6]/a[1]

Clicar no botao para inserir um novo motorista
   Click Button        xpath=/html[1]/body[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/button[2]
   Maximize Browser window

Inserir um nome para o motorista
   ${resp_nome}=  Set Suite Variable   ${nome}
   Input Text   xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/form/div[1]/div[2]/div[1]/div/div/div/div/div[3]/div/div/div/input     ${nome}
  
Inserir um cpf para o motorista
   ${resp_cpf}=  Set Suite Variable   ${cpf}
   Input Text   xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/form/div[1]/div[2]/div[1]/div/div/div/div/div[5]/div[1]/div/div/input  ${cpf}

Inserir um Rg
   ${resp_rg}=  Set Suite Variable   ${rg}  
   Input Text   xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/form/div[1]/div[2]/div[1]/div/div/div/div/div[5]/div[2]/div/div/input     ${rg}

Inserir nome da mae
   Input Text  xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/form/div[1]/div[2]/div[1]/div/div/div/div/div[6]/div[1]/div/div/input   Maria 

Inserir nome do pai
   Input Text  xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/form/div[1]/div[2]/div[1]/div/div/div/div/div[6]/div[2]/div/div/input   João   

Orgao expedidor
  Input Text   xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/form/div[1]/div[2]/div[1]/div/div/div/div/div[7]/div[1]/div/div/input   SC

Numero cnh
  Input Text   xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/form/div[1]/div[2]/div[1]/div/div/div/div/div[7]/div[2]/div/div/input  ${cnh}

Inserir CNH
  Input Text   xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/form/div[1]/div[2]/div[1]/div/div/div/div/div[7]/div[2]/div/div/input    ${cnh}

  
*** Keywords ***

Clicar na categoria da cnh
  Click Element   xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/form/div[1]/div[2]/div[1]/div/div/div/div/div[8]/div[1]/div/div/div/div[1]/span

Escolher a categoria da cnh
  Click Element   xpath=//*[@id="ui-select-choices-row-0-5"]/span

Clicar na data
  Input Text   xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/form/div[1]/div[2]/div[1]/div/div/div/div/div[8]/div[2]/div/div/p/input   12/10/2026

Inserir um numero de celular
  Input Text   xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/form/div[1]/div[2]/div[1]/div/div/div/div/div[9]/div[2]/div/div/input     48999252638

Data d nascimento do motora
  Input Text   xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/form/div[1]/div[2]/div[1]/div/div/div/div/div[10]/div[2]/div/div/p/input   10/10/1980

Acessar o campo endereço
  Click Element   xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/form/div[1]/div[1]/ol/li[2]

Informar o cep
  Input Text     xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/form/div[1]/div[2]/div[2]/div/div/div/div[1]/div/div/input   88036140

Clicar fora do campo para carregar as informações
  Click Element   xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/form/div[1]/div[1]/ol/li[2]

Clicar no botão avaliações
  Click Element   xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/form/div[1]/div[1]/ol/li[4]
   sleep  2s  
   Set Selenium Implicit Wait  4s
Salvar o motorista
  Click Element   xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/form/div[3]/div/div/button[3]