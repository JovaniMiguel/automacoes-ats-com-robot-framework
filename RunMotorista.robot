*** Settings ***
Documentation   swite acessar 4fordevs e cadastrar um motorista
Resource        CadastroMotorista.robot


*** Test Cases *** 
Cenario de teste 01: Acessar o site do govervo e gerar uma CNH e escrever o valor no console

  Abrir o site do gerador de CNH

  Clicar no botao para gerar cnh

  copiar a cnh eescrever no console

Cenario de teste 02: Acessar o site 4fordevs e pegar as informações para cadastro de motorista
  Acessar a pagina do site do 4fordevs

  Clicar no botao gerador de pessoas

  Clicar no checkbox para não gerar com pntuação

  Clicar no checkbox para criar um usuário do masculino

  Clicar no botão gerar pessoa

  Resgatar o nome

  Resgatar o cpf

  Resgatar o RG

  Passar valor da variavel para o campo



Cenario de teste 03: Acessar site do ats e cadastrar um motorista

  Acessar a pagina home do ATS

  Clicar no botão Cokies
  
  Digitar o nome "empresa.supercerta" na Pesquisa

  Informar a senha "sistema.1234" no campo senha

  Clicar no botão subimit
  
  Clicar no botão config

  Clicar no botão cadastros

  Acessar o campo motoristas
  
  Clicar no campo para cadastrar

  Clicar no botao para inserir um novo motorista
  
  Inserir um nome para o motorista

  Inserir um cpf para o motorista

  Inserir um Rg

  Inserir nome da mae
  
  Inserir nome do pai

  Orgao expedidor

  Inserir CNH

Cenario de teste 04: Inserir e data de nascimento e passar para os proximos campos

  Clicar na categoria da cnh

  Escolher a categoria da cnh

  Clicar na data

  Inserir um numero de celular

  Data d nascimento do motora

  Acessar o campo endereço

  Informar o cep

  Clicar fora do campo para carregar as informações

  Clicar no botão avaliações

  Salvar o motorista



