*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${BROWSER}    chrome
${URL}        http://atsweb.sistemainfo.com.br:8024/login

*** Keywords ***
Acessar a pagina inicial do ATS
  Open Browser   url=${URL}   browser=${BROWSER} 

Clicar no botão Cokies
  Click Element       xpath=(.//*[normalize-space(text()) and normalize-space(.)='Lei Geral de Proteção de Dados'])[2]/following::button[1]

Digitar o nome "${USUARIO}" na Pesquisa
  Input Text        xpath=/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/form[1]/input[1]   ${USUARIO}

Informar a senha "${Senha login}" no campo senha
   Input Text          xpath=/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/form[1]/input[2]    ${Senha login}

Clicar no botão subimit
   Click Button        xpath=/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/form[1]/button[1]
   sleep  5s
   Set Selenium Implicit Wait  8s

Clicar no botão config
   Click Element      xpath=/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/nav[1]/div[1]/span[1]/div[1]/ul[1]/li[2]/a[1]/span[1]
   Maximize Browser window 
Clicar no botão cadastros
   Click Element      xpath=/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/nav[1]/div[1]/span[1]/div[1]/ul[1]/li[2]/ul[1]/li[1]/a[1]
    sleep  2s  
   Set Selenium Implicit Wait  4s
Abrir a barra do menu
   Click Element      xpath=/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/nav[1]/div[1]/span[1]/div[1]/ul[1]/li[2]/ul[1]/li[1]/a[1]

Acessar o campo Proprietário
   Click Element      xpath=//*[@id="side-menu"]/li[2]/ul/li/ul/li[11]/a
   

Clicar no botao para inserir um novo Proprietário
   Click Button        xpath=//*[@id="page-wrapper"]/div[2]/div/div/div/div/div/div/div[2]/div[1]/div[1]/button[2]
   











# # Acessar a pagina home do site
# #    Open Browser  url=${URL}   browser=${BROWSER} 

# # Clicar no botão Cokies
# #   Click Element       xpath=(.//*[normalize-space(text()) and normalize-space(.)='Lei Geral de Proteção de Dados'])[2]/following::button[1]

# # Digitar o nome "${USUARIO}" na Pesquisa
# #   Input Text        xpath=/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/form[1]/input[1]  ${USUARIO}

# # Informar a senha "${Senha login}" no campo senha
# #    Input Text          xpath=/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/form[1]/input[2]    ${Senha login}
  
# # Clicar no botão subimit
# #    Click Button   xpath=/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/form[1]/button[1] 
   
# # Esperar segundos
# #    Sleep 4

#   #  Set Selenium Speed 0.5 seconds
    

# Clicar no botão config
#   #  Click Element    xpath=//*[@id="side-menu"]/li[2]/a/span[1]
#   #  Click Element    xpath=/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/nav[1]/div[1]/span[1]/div[1]/ul[1]/li[2]/a[1]/span[1]
#   #  Click Element    xpath=(//span[normalize-space()='Configuração'])[1]
#   #  Click Element    xpath=//span[normalize-space()='Configuração']
#   # Click Element        xpath=//a[contains(text(),'Configuração')]
    
#     Click Element      xpath=//*[@id="side-menu"]/li[2]/a/span[1]
  